#first test class
class first {
  $user = 'ec3-test'
  user { $user:
    ensure => 'present',
    home   => "/tmp/${user}",
    shell  => '/bin/false',
    #group => $user,
  }
  file { "/tmp/${user}":
    ensure => directory,
    owner  => $user,
    group  => $user,
  }
  file { "/tmp/${user}/test":
    ensure => directory,
    owner  => $user,
    group  => $user,
  }
  file { "/tmp/${user}/test/test1.txt":
    mode   => '0644',
    owner  => $user,
    group  => $user,
    source => 'puppet:///modules/first/test.txt',
  }
  file { "/tmp/${user}/test/test2.txt":
    mode    => '0644',
    owner   => $user,
    group   => $user,
    content => epp ( 'first/test1.txt', {
      user => $user,
    } )
  }
  file { "/tmp/${user}/test/test3.txt":
    mode    => '0644',
    owner   => $user,
    group   => $user,
    content => $::osfamily,
  }



}
