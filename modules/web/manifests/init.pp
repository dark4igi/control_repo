#web class
class web {
  package { 'httpd':
    ensure => installed,
    }

  file { [ '/var/www/',
        '/var/www/html/']:
    ensure => "directory",
    recurse => true,
  }

  file { "/var/www/html/index.html":
    mode   => '0644',
    source => 'puppet:///modules/web/index.html',
    require => File['/var/www/html/'],
    notify => Service[httpd],
  }

  service { 'httpd' :
    ensure => running,
  }
}
