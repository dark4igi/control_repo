#base class
class base {
  $packages = [ 'jq', 'htop', 'iotop', 'mc', 'vim' ]

  package { $packages:
    ensure => 'latest',
  }
  file { '/etc/hosts':
    ensure => 'present',
    source => 'puppet:///modules/base/hosts.rhel',
  }

}
