package { 'ntp':
  ensure => 'latest',
}

file { '/etc/ntp.conf':
  ensure  => 'present',
  content => "server 0.centos.pool.ntp.org iburst\n",
# source => file("ntp.conf"),
}

service { 'ntpd':
  ensure => 'running',
  enable => true,
}
