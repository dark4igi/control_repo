file { '/tmp/test.txt':
  ensure => 'present',
  owner  => 'ec2-user',
  group  => 'ec2-user',
  mode   => '0777',
}
