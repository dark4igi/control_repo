#! /bin/bash

set -x

echo '127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4' >> /etc/puppetlabs/code/environments/production/modules/base/files/hosts.rhel
echo '::1         localhost6 localhost6.localdomain6' >> /etc/puppetlabs/code/environments/production/modules/base/files/hosts.rhel
echo '####################' >> /etc/puppetlabs/code/environments/production/modules/base/files/hosts.rhel
aws ec2 describe-instances --query 'Reservations[*].Instances[*].[Tags[?Key==`Name`].Value,PrivateIpAddress]' --output=text | paste - - >> /etc/puppetlabs/code/environments/production/modules/base/files/hosts.rhel
