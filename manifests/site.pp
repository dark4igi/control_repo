user { 'yoda':
  ensure => 'absent',
}

file { '/tmp/test.txt':
  ensure => 'absent',
}
include first
include base
include web

case $::osfamily {
  'RedHat': {
    file { '/tmp/os.test':
      ensure  => 'present',
      content => 'RHEL'
    }
  }

  'Debian': {
    file { '/tmp/os.test':
      ensure  => 'present',
      content => "deb\n",
    }
  }

  'default': {
    file {'/tmp/os.test':
      ensure  => 'present',
      content => 'default',
    }
  }

}

